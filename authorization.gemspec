$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "authorization/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "authorization"
  s.version     = Authorization::VERSION
  s.authors     = ["Kevin Madhu"]
  s.email       = ["kevin.madhu@gmail.com"]
  s.homepage    = "https://gitlab.com/ligs-informatics/openion-authorization/wikis/home"
  s.summary     = "Authorization-plugin"
  s.description = "This is an authorization plugin for OpenionERP."
  s.license     = "MIT"
  
  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.0"

  s.add_development_dependency "sqlite3"
end
