Authorization::Engine.routes.draw do
  resources :roles, except: [:show] do
    get 'assign_users'
    get 'assign_permissions'
  end
end
