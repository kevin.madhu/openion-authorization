class CreateAuthorizationActions < ActiveRecord::Migration
  def change
    create_table :authorization_actions do |t|
      t.belongs_to :activity, index: true
      t.string :name
      t.string :desc

      t.timestamps null: false
    end
  end
end
