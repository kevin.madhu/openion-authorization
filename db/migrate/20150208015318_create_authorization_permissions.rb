class CreateAuthorizationPermissions < ActiveRecord::Migration
  def change
    create_table :authorization_permissions do |t|
      t.belongs_to :role, index: true
      t.belongs_to :action, index: true
    end
  end
end
