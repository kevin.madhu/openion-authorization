class CreateAuthorizationRoles < ActiveRecord::Migration
  def change
    create_table :authorization_roles do |t|
      t.string  :name
      t.boolean :is_basic, default: false
      
      t.timestamps null: false
    end

    create_table :authorization_duties do |t|
      t.belongs_to :user, index: true
      t.belongs_to :role, index: true
      t.timestamps null: false
    end
  end
end
