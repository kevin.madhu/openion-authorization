class CreateAuthorizationActivities < ActiveRecord::Migration
  def change
    create_table :authorization_activities do |t|
      t.string :scenario
      t.timestamps null: false
    end
  end
end
