namespace :authorize do
  desc "Load all activities of the app, required for authorization"
  task :load_activities => :environment do
    activities = app_activities
    activities.keys.sort.each do |key|
      scenario = key.to_s
      @activity = Authorization::Activity.find_by scenario: scenario
      @activity = Authorization::Activity.create!(scenario: scenario) if @activity.nil?
      actions = activities[key].uniq.sort
      actions.each do |action|
        Authorization::Action.create!(activity: @activity, name: action)
      end
    end
  end

  desc "Load all activities of the app, required for authorization"
  task :reload_activities => :environment do
    Authorization::Activity.delete_all
    Authorization::Action.delete_all
    Rake::Task["authorize:load_activities"].invoke
  end

  ## Helper methods for the tasks

  # Returns a list of all resources and actions using routes
  def routes_list engine
    routes = engine.routes.routes.map do |route|
      { alias: route.name, controller: route.defaults[:controller],
      action: route.defaults[:action] }
     end
  end

  # Converts obtained routing info to desired format
  def app_activities
    blacklisted_activities = ['rails/welcome', 'rails/info', 'rails/mailers',
      'devise', 'devise/sessions','devise/passwords', 
      'authorization/permissions', 'authorization/roles', 'users']

    engines = []
    activities = {}
    routes_list(Rails.application).each do |route|
      if route[:controller].nil?
        engines << route[:alias] unless route[:alias].nil?
      else
        controller, action = route[:controller].to_sym, route[:action]
        unless blacklisted_activities.include? controller.to_s
          activities[controller] = [] unless activities.has_key? controller
          activities[controller] << action
        end
      end
    end

    engines.each do |engine|
      @engine = (engine.capitalize + "::Engine").constantize
      routes_list(@engine).each do |route|
        controller, action = route[:controller].to_sym, route[:action]
        unless blacklisted_activities.include? controller.to_s
          activities[controller] = [] unless activities.has_key? controller
          activities[controller] << action
        end
      end
    end
    activities
  end
end