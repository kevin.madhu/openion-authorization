module Authorization
  class Duty < ActiveRecord::Base
    belongs_to :user
    belongs_to :role
  end
end
