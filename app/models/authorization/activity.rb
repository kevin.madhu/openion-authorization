module Authorization
  class Activity < ActiveRecord::Base
    has_many :actions
    has_many :permissions
    has_many :roles, through: :permissions

    def to_s
      desc or scenario.capitalize
    end

    def actions_list
      actions = []
      real_actions = {}
      
      self.actions.each do |action|
        real_actions[action.to_s] = action.id
      end

      temp = []
      temp << real_actions["Index"] if real_actions.has_key? "Index"
      temp << real_actions["Show"] if real_actions.has_key? "Show"
      actions << {name:"Read Permission", id: temp.join(", ")} unless temp.empty?
      
      temp = []
      temp << real_actions["New"] if real_actions.has_key? "New"
      temp << real_actions["Create"] if real_actions.has_key? "Create"
      actions << {name:"Write Permission", id: temp.join(", ")} unless temp.empty?

      temp = []
      temp << real_actions["Edit"] if real_actions.has_key? "Edit"
      temp << real_actions["Update"] if real_actions.has_key? "Update"
      actions << {name:"Update Permission", id: temp.join(", ")} unless temp.empty?

      actions << {name:"Remove Permission", id: real_actions["Destroy"]} if real_actions.has_key? "Destroy"
      real_actions.tap do |hs| 
        hs.delete("Index")
        hs.delete("Show")

        hs.delete("New")
        hs.delete("Create")

        hs.delete("Edit")
        hs.delete("Update")

        hs.delete("Destroy")
      end
      real_actions.keys.each do |key|
        actions << {name: key.to_s.capitalize + " Permission", id: real_actions[key]}
      end
      actions
    end
  end
end
