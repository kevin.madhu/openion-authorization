module Authorization
  class Role < ActiveRecord::Base
    has_many :duties
    has_many :permissions
    has_many :users, class_name: "User", through: :duties
    has_many :actions, through: :permissions

    def to_s
      name.capitalize
    end
  end
end
