module Authorization
  class Action < ActiveRecord::Base
    belongs_to :activity

    def to_s
      desc or name.capitalize
    end
  end
end
