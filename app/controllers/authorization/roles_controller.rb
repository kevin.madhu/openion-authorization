require_dependency "authorization/application_controller"

module Authorization
  class RolesController < ApplicationController
    before_action :set_role, only: [:edit, :update, :destroy]

    # GET /roles
    def index
      @roles = policy_scope(Role.all)
      authorize @roles
    end

    # GET /roles/new
    def new
      @role = Role.new
      authorize(@role)
    end

    # GET /roles/1/edit
    def edit
    end

    # POST /roles
    def create
      @role = Role.new(role_params)
      authorize(@role)

      if @role.save
        redirect_to roles_url, notice: 'Role was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /roles/1
    def update
      params = role_params
      if role_params[:action_ids].present?
        action_ids = []
        role_params[:action_ids].each do |ids|
          ids.split(", ").each do |id|
            action_ids << id
          end
        end
        params[:action_ids] = action_ids
      end

      if @role.update(params)
        redirect_to roles_url, notice: 'Role was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /roles/1
    def destroy
      if @role.is_basic
        redirect_to roles_url, :flash => { :error => "The role cannot be removed." }
      else
        @role.destroy
        redirect_to roles_url, notice: 'Role was successfully destroyed.'
      end
    end

    def assign_users
      @role = Role.find(params[:role_id])
      authorize(@role)
    end

    def assign_permissions
      @role = Role.find(params[:role_id])
      authorize(@role)
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_role
        @role = Role.find(params[:id])
        authorize(@role)
      end

      # Only allow a trusted parameter "white list" through.
      def role_params
        params.require(:role).permit(:name, :is_basic, user_ids: [], action_ids:[])
      end
  end
end
